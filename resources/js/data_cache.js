var _menus = {
	basic : [ {
		"menuid" : "110",
		"icon" : "icon-sys",
		"menuname" : "常用功能",
		"menus" : [ {
			"menuid" : "111",
			"menuname" : "百度",
			"icon" : "icon-nav",
			"url" : "http://www.baidu.com"
		}, {
			"menuid" : "113",
			"menuname" : "腾讯官网",
			"icon" : "icon-nav",
			"url" : "http://www.qq.com"
		}, {
			"menuid" : "115",
			"menuname" : "CSDN",
			"icon" : "icon-nav",
			"url" : "http://www.csdn.net/"
		}, {
			"menuid" : "117",
			"menuname" : "cnBeta",
			"icon" : "icon-nav",
			"url" : "http://www.cnbeta.com/"
		}]
	}, {
		"menuid" : "121",
		"icon" : "icon-sys",
		"menuname" : "基础功能",
		"menus" : [ {
			"menuid" : "1211",
			"menuname" : "字典表管理",
			"icon" : "icon-nav",
			"url" : "#"
		}]
	}, {
		"menuid" : "122",
		"icon" : "icon-sys",
		"menuname" : "用户管理",
		"menus" : [ {
			"menuid" : "1221",
			"menuname" : "新增用户",
			"icon" : "icon-nav",
			"url" : "#"
		}, {
			"menuid" : "1222",
			"menuname" : "权限管理",
			"icon" : "icon-nav",
			"url" : "#"
		} ]
	}, {
		"menuid" : "123",
		"icon" : "icon-sys",
		"menuname" : "系统设置",
		"menus" : [ {
			"menuid" : "1231",
			"menuname" : "菜单管理",
			"icon" : "icon-nav",
			"url" : "views/menu/menu.html"
		},{
			"menuid" : "1232",
			"menuname" : "用户管理",
			"icon" : "icon-nav",
			"url" : "views/user/user.html"
		},{
			"menuid" : "1233",
			"menuname" : "用户管理2",
			"icon" : "icon-nav",
			"url" : "views/user/user2.html"
		},{
			"menuid" : "1234",
			"menuname" : "权限管理",
			"icon" : "icon-nav",
			"url" : "views/user/user2.html"
		},{
			"menuid" : "1234",
			"menuname" : "菜单组管理",
			"icon" : "icon-nav",
			"url" : "views/user/user2.html"
		}]
	}],
	point : [{
		"menuid" : "220",
		"icon" : "icon-sys",
		"menuname" : "积分管理",
		"menus" : [ {
			"menuid" : "211",
			"menuname" : "积分用途",
			"icon" : "icon-nav",
			"url" : "#"
		}, {
			"menuid" : "213",
			"menuname" : "积分调整",
			"icon" : "icon-nav",
			"url" : "#"
		} ]

	}],
	easyui:[{
		"menuid" : "310",
		"icon" : "icon-sys",
		"menuname" : "accordion",
		"menus" : [ {
			"menuid" : "101",
			"menuname" : "accordion_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/basic.html"
		}, {
			"menuid" : "102",
			"menuname" : "accordion_actions",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/actions.html"
		}, {
			"menuid" : "103",
			"menuname" : "accordion_ajax",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/ajax.html"
		}, {
			"menuid" : "104",
			"menuname" : "accordion_expandable",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/expandable.html"
		} , {
			"menuid" : "105",
			"menuname" : "accordion_fluid",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/fluid.html"
		}, {
			"menuid" : "106",
			"menuname" : "accordion_multiple",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/multiple.html"
		}, {
			"menuid" : "107",
			"menuname" : "accordion_tools",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/accordion/tools.html"
		}]	
	},{
		"menuid" : "320",
		"icon" : "icon-sys",
		"menuname" : "calendar",
		"menus" : [ {
			"menuid" : "201",
			"menuname" : "calendar_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/calendar/basic.html"
		},{
			"menuid" : "202",
			"menuname" : "calendar_custom",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/calendar/custom.html"
		},{
			"menuid" : "203",
			"menuname" : "calendar_disabledate",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/calendar/disabledate.html"
		},{
			"menuid" : "204",
			"menuname" : "calendar_firstday",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/calendar/firstday.html"
		},{
			"menuid" : "205",
			"menuname" : "calendar_fluid",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/calendar/fluid.html"
		}]
	},{
		"menuid":"330",
		"icon":"icon-sys",
		"menuname":"combo",
		"menus":[{
			"menuid" : "301",
			"menuname" : "calendar_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combo/basic.html"
		},{
			"menuid" : "302",
			"menuname" : "calendar_animation",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combo/animation.html"
		}]
	},{
		"menuid":"340",
		"icon":"icon-sys",
		"menuname":"combobox",
		"menus":[{
			"menuid" : "401",
			"menuname" : "calendar_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/basic.html"
		},{
			"menuid" : "402",
			"menuname" : "calendar_animation",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/actions.html"
		},{
			"menuid" : "403",
			"menuname" : "calendar_customformat",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/customformat.html"
		},{
			"menuid" : "404",
			"menuname" : "calendar_dynamicdata",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/dynamicdata.html"
		},{
			"menuid" : "405",
			"menuname" : "calendar_fluid",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/fluid.html"
		},{
			"menuid" : "406",
			"menuname" : "calendar_group",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/group.html"
		},{
			"menuid" : "407",
			"menuname" : "calendar_icons",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/icons.html"
		},{
			"menuid" : "408",
			"menuname" : "calendar_itemicon",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/itemicon.html"
		},{
			"menuid" : "409",
			"menuname" : "calendar_multiline",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/multiline.html"
		},{
			"menuid" : "410",
			"menuname" : "calendar_multiple",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/multiple.html"
		},{
			"menuid" : "411",
			"menuname" : "calendar_navigation",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/navigation.html"
		},{
			"menuid" : "412",
			"menuname" : "calendar_remotedata",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/remotedata.html"
		},{
			"menuid" : "413",
			"menuname" : "calendar_remotejsonp",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combobox/remotejsonp.html"
		}]
	},{
		"menuid":"350",
		"icon":"icon-sys",
		"menuname":"combogrid",
		"menus":[{
			"menuid" : "3501",
			"menuname" : "combogrid_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/basic.html"
		},{
			"menuid" : "3502",
			"menuname" : "combogrid_actions",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/actions.html"
		},{
			"menuid" : "3503",
			"menuname" : "combogrid_fluid",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/fluid.html"
		},{
			"menuid" : "3504",
			"menuname" : "combogrid_initvalue",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/initvalue.html"
		},{
			"menuid" : "3505",
			"menuname" : "combogrid_multiple",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/multiple.html"
		},{
			"menuid" : "3506",
			"menuname" : "combogrid_navigation",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/navigation.html"
		},{
			"menuid" : "3507",
			"menuname" : "combogrid_setvalue",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combogrid/setvalue.html"
		}]
	},{
		"menuid":"351",
		"icon":"icon-sys",
		"menuname":"combotree",
		"menus":[{
			"menuid" : "3511",
			"menuname" : "combotree_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combotree/basic.html"
		},{
			"menuid" : "3512",
			"menuname" : "combotree_actions",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combotree/actions.html"
		},{
			"menuid" : "3513",
			"menuname" : "combotree_fluid",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combotree/fluid.html"
		},{
			"menuid" : "3514",
			"menuname" : "combotree_initvalue",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combotree/initvalue.html"
		},{
			"menuid" : "3515",
			"menuname" : "combotree_multiple",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/combotree/multiple.html"
		}]
	},{
		"menuid":"352",
		"icon":"icon-sys",
		"menuname":"datagrid",
		"menus":[{
			"menuid" : "3521",
			"menuname" : "datagrid_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/basic.html"
		},{
			"menuid" : "3522",
			"menuname" : "datagrid_aligncolumns",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/aligncolumns.html"
		},{
			"menuid" : "3523",
			"menuname" : "datagrid_cacheeditor",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/cacheeditor.html"
		},{
			"menuid" : "3524",
			"menuname" : "datagrid_cellstyle",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/cellstyle.html"
		},{
			"menuid" : "3525",
			"menuname" : "datagrid_checkbox",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/checkbox.html"
		},{
			"menuid" : "3526",
			"menuname" : "datagrid_clientpagination",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/clientpagination.html"
		},{
			"menuid" : "3527",
			"menuname" : "datagrid_columngroup",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/columngroup.html"
		},{
			"menuid" : "3528",
			"menuname" : "datagrid_complextoolbar",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/complextoolbar.html"
		},{
			"menuid" : "3529",
			"menuname" : "datagrid_contextmenu",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/contextmenu.html"
		},{
			"menuid" : "3530",
			"menuname" : "datagrid_custompager",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/custompager.html"
		},{
			"menuid" : "3531",
			"menuname" : "datagrid_fluid",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/fluid.html"
	    },{
			"menuid" : "3532",
			"menuname" : "datagrid_footer",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/footer.html"
		},{
			"menuid" : "3533",
			"menuname" : "datagrid_formatcolumns",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/formatcolumns.html"
		},{
			"menuid" : "3534",
			"menuname" : "datagrid_frozencolumns",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/frozencolumns.html"
		},{
			"menuid" : "3535",
			"menuname" : "datagrid_frozenrows",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/frozenrows.html"
		},{
			"menuid" : "3536",
			"menuname" : "datagrid_mergecells",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/mergecells.html"
		},{
			"menuid" : "3537",
			"menuname" : "datagrid_multisorting",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/multisorting.html"
		},{
			"menuid" : "3538",
			"menuname" : "datagrid_rowborder",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/rowborder.html"
		},{
			"menuid" : "3539",
			"menuname" : "datagrid_rowediting",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/rowediting.html"
		},{
			"menuid" : "3540",
			"menuname" : "datagrid_rowstyle",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/rowstyle.html"
		},{
			"menuid" : "3541",
			"menuname" : "datagrid_selection",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/selection.html"
		},{
			"menuid" : "3542",
			"menuname" : "datagrid_simpletoolbar",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/simpletoolbar.html"
		},{
			"menuid" : "3543",
			"menuname" : "datagrid_transform",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datagrid/transform.html"
		}]
	},{
		"menuid":"353",
		"icon":"icon-sys",
		"menuname":"datalist",
		"menus":[{
			"menuid" : "3531",
			"menuname" : "datalist_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/basic.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_checkbox",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/checkbox.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_group",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/group.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_multiselect",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/multiselect.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_remotedata",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/remotedata.html"
		}]
	},{
		"menuid":"353",
		"icon":"icon-sys",
		"menuname":"datalist",
		"menus":[{
			"menuid" : "3531",
			"menuname" : "datalist_basic",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/basic.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_checkbox",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/checkbox.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_group",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/group.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_multiselect",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/multiselect.html"
		},{
			"menuid" : "3532",
			"menuname" : "datalist_remotedata",
			"icon" : "icon-nav",
			"url" : "resources/plugins/easyui/demo/datalist/remotedata.html"
		}]
	},{
		"menuid":"354",
		"icon":"icon-sys",
		"menuname":"EasyUI资料",
		"menus":[{
			"menuid" : "3541",
			"menuname" : "EasyUI(API)",
			"icon" : "icon-nav",
			"url" : "http://www.jeasyui.com/documentation/index.php"
		},{
			"menuid" : "3542",
			"menuname" : "EasyUI(Demo)",
			"icon" : "icon-nav",
			"url" : "http://www.jeasyui.com/demo/main/index.php"
		}]
	}]
}